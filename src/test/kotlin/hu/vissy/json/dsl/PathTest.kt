package hu.vissy.json.dsl

import org.testng.Assert.assertEquals
import org.testng.annotations.Test

class PathTest {

    private val json1 = jsonObject {
        "a" += "apple"
        "b" += 42
        "c" += jsonObject {
            "s" += "sof"
            "x" += jsonArray {
                +"1"
                +"2"
                +"3"
            }
            "y" += jsonObject {
                "alpha" += jsonObject {
                    "multiArray" += jsonArray {
                        +jsonArray {
                            +"a1"
                            +"a2"
                        }
                    }
                }
            }
        }
        "arr" += jsonArray {
            +"i1"
            add(2)
            +"i3"
        }


    }

    @Test
    fun whenQueryingAnObjectField_itIsReturned() {
        assertEquals(json1.strictPath("a").asString, "apple")
        assertEquals(json1.strictPath("b").asInt, 42)
    }

    @Test
    fun whenQueryingAnArrayValue_itIsReturned() {
        assertEquals(json1.strictPath("arr[0]").asString, "i1")
        assertEquals(json1.strictPath("arr[1]").asInt, 2)
    }

    @Test
    fun whenQueryingASubobjectField_itIsReturned() {
        assertEquals(json1.strictPath("c.s").asString, "sof")
        assertEquals(json1.strictPath("c.x[1]").asInt, 2)
    }

    @Test
    fun whenQueryingMultiDimensionalArrays_itIsHandledCorrectly() {
        assertEquals(json1.strictPath("c.y.alpha.multiArray[0][0]").asString, "a1")
        assertEquals(json1.strictPath("c.y.alpha.multiArray[0,1]").asString, "a2")
    }

    @Test
    fun forDoc() {
        // Creating a new Json
        val json1 = jsonObject {
            "f1" += 42
            "f2" += "apple"
        }

        println(json1)

        // Extending the json
        val json2 = jsonObject(json1) {
            // You can do anything within
            val v = (Math.random() * 100).toInt()
            "f3" += v
            "f4" += jsonArray {
                // You can iterate
                (0..4).forEach {
                    +"i$it"
                }
            }
        }

        println(json2)

        println(json2.toPretty())

        val v: Int? = json2("f5", optional)
        println(v)
    }

}