@file:Suppress("unused", "MemberVisibilityCanBePrivate", "ClassName", "UNUSED_PARAMETER")

package hu.vissy.json.dsl

import com.google.gson.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.reflect.KClass

/**
 * Creates or updates a JsonObject. If the [obj] is not specified, a new object will be
 * created, but when [obj] is provided it is extended with the new members.
 * To add a member to the JSON, use the `+=` operator with a String as left side operand.
 * For example:
 *
 * ```
 * val json1 = jsonObject {
 *    "a" += 42
 * }
 * val json2 = jsonObject(json1) {
 *    "b" += Math.round(4.2)
 * }
 *
 * println(json2)
 * ```
 * will produce:
 *
 * ```
 * { "a" : 42, "b" : 4 }
 * ```
 */
fun jsonObject(obj: JsonObject = JsonObject(), op: JsonObjectBuilder.() -> Unit) =
    JsonObjectBuilder(obj).apply(op).build()

/**
 * Creates or updates a JsonArray. If the [arr] is not specified, a new array will be
 * created, but when [arr] is provided it is extended with the new members.
 * To add a member to the JSON, use the single-operand (prefix) `+` operator or the [JsonArrayBuilder.add]
 * function.
 *
 * **Note**: Due to the original function of the operator, for numeric values only the
 * `add` function could be used.
 *
 * For example:
 *
 * ```
 * val json1 = jsonArray {
 *    +"a"
 *    add(42)
 * }
 * val json2 = jsonArray(json1) {
 *    +Math.round(4.2)
 * }
 *
 * println(json2)
 * ```
 * will produce:
 *
 * ```
 * [ "a", 42, 4 ]
 * ```
 */
fun jsonArray(arr: JsonArray = JsonArray(), op: JsonArrayBuilder.() -> Unit) = JsonArrayBuilder(arr).apply(op).build()

/**
 * Creates a JsonArray containing each values from [items].
 */
fun jsonArray(items: Iterable<Any?>): JsonArray {
    val res = JsonArray()
    items.forEach { addToArray(res, it) }

    return res
}


/**
 * Creates a JsonArray containing each arguments.
 */
fun jsonArray(vararg items: Any?): JsonArray {
    val res = JsonArray()
    items.forEach { addToArray(res, it) }
    return res
}

private fun addToArray(res: JsonArray, it: Any?) {
    if (it == null) res.add(JsonNull.INSTANCE)
    else when (it) {
        is String -> res.add(JsonPrimitive(it))
        is Number -> res.add(JsonPrimitive(it))
        is Boolean -> res.add(JsonPrimitive(it))
        is JsonObject -> res.add(it)
        is JsonArray -> res.add(it)
        else -> res.add(JsonPrimitive(it.toString()))
    }
}

/**
 * Backbone class for populating a JSON object.
 *
 * This class is rarely instantiated directly. Most commonly the [jsonObject] builder function is used to
 * create the JSON object.
 */
class JsonObjectBuilder(private val obj: JsonObject = JsonObject()) {

    /**
     * Adds the [value] to the object with label [this].
     */
    infix operator fun String.plusAssign(value: Int) = obj.addProperty(this, value)

    /**
     * Adds the [value] to the object with label [this].
     */
    infix operator fun String.plusAssign(value: Long) = obj.addProperty(this, value)

    /**
     * Adds the [value] to the object with label [this].
     */
    infix operator fun String.plusAssign(value: Boolean) = obj.addProperty(this, value)

    /**
     * Adds the [value] to the object with label [this].
     */
    infix operator fun String.plusAssign(value: String) = obj.addProperty(this, value)

    /**
     * Adds the [value] to the object with label [this].
     */
    infix operator fun String.plusAssign(value: Double) = obj.addProperty(this, value)

    /**
     * Adds the [value] to the object with label [this].
     */
    infix operator fun String.plusAssign(value: JsonElement) = obj.add(this, value)

    /**
     * Adds the string representation of [value] to the object with label [this]. If the object is null, null
     * will be added.
     */
    infix operator fun String.plusAssign(value: Any?) =
        if (value == null)
            obj.add(this, JsonNull.INSTANCE)
        else obj.addProperty(this, value.toString())

    /**
     * Creates a sub object and populates it by [op] with label [this].
     */
    operator fun String.invoke(op: (JsonObjectBuilder) -> Unit) =
        obj.add(this, JsonObjectBuilder().apply { op(this) }.build())

    /**
     * Builds the JsonObject.
     *
     * **Note:** This function is rarely used directly. (Better to use [jsonObject].) However, if this is called manually
     * it is important to be aware of that the returned JSON object is not immutable by the builder, so if you return
     * an object and then the builder is called to add more fields, the previously returned object will change as well.
     */
    fun build() = obj
}

/**
 * Backbone class for populating a JSON array.
 *
 * This class is rarely instantiated directly. Most commonly the [jsonArray] builder function is used to
 * create the JSON array.
 */
class JsonArrayBuilder(private val arr: JsonArray = JsonArray()) {

    /**
     * Adds [other] as a numeric value to the JSON array.
     */
    fun add(other: Number) = arr.add(JsonPrimitive(other))

    /**
     * Adds [this] as a boolean value to the JSON array.
     */
    operator fun Boolean.unaryPlus() = arr.add(JsonPrimitive(this))

    /**
     * Adds [this] as a string value to the JSON array.
     */
    operator fun String.unaryPlus() = arr.add(JsonPrimitive(this))

    /**
     * Adds a null value to the JSON array.
     */
    @Suppress("FunctionName")
    fun NULL() = arr.add(JsonNull.INSTANCE)

    /**
     * Adds [this] as a sub structure (object or array) to the JSON array.
     */
    operator fun JsonElement.unaryPlus() = arr.add(this)

    /**
     * Adds the string representation of [this]  to the JSON array. If [this] is null, a null value will
     * be added.
     */
    operator fun Any?.unaryPlus() = if (this == null) NULL() else arr.add(JsonPrimitive(this.toString()))

    /**
     * Builds the JsonArray.
     *
     * **Note:** This function is rarely used directly. (Better to use [jsonArray].) However, if this is called manually
     * it is important to be aware of that the returned JSON array is not immutable by the builder, so if you return
     * an object and then the builder is called to add more items, the previously returned array will change as well.
     */
    fun build() = arr
}


/**
 * Formats [this]. It spits the values in lines and indents the code. Uses 4 spaces as indent.
 */
@Suppress("RegExpRedundantEscape")
fun JsonElement.toPretty() =
    GsonBuilder().setPrettyPrinting().create().toJson(this)
        .replace("\n".toRegex(), "ß") // Replace newlines to allow over-the-line search
        .replace("\\\"\\,ß([ ]*)\\\"".toRegex(), "\",$1\"") // Replace consecutive string elements to be in one line
        .replace("\\,([ ]*)(\\\"[^\"]*\\\"):".toRegex(), ",ß$1$2:") // For object keys, the newline marker is restores
        .replace("\\,[ ]*\\\"".toRegex(), ", \"") // Remove unnecesary spaces
        .replace(" {2}".toRegex(), "    ") // Indent 4, instead of 2
        .replace("ß".toRegex(), "\n")

/**
 * Returns the value of the JSON object field as type [T]. The field should exist and could not be null.
 */
inline operator fun <reified T : Any> JsonObject.invoke(key: String) = this.internalGetOrDefault(T::class, key)
    ?: error("Json field '$key' should not be null.")

/**
 * Returns the value of the JSON object field as type [T]. If the field doesn't exist or null, the [default]
 * is returned.
 */
inline operator fun <reified T : Any> JsonObject.invoke(key: String, default: T) =
    this.internalGetOrDefault(T::class, key, default)
        ?: default

/**
 * Marker object for [invoke].
 */
object optional

/**
 * Returns the value of the JSON object field as type [T]. If the field doesn't exist null is returned.
 */
inline operator fun <reified T : Any> JsonObject.invoke(key: String, optional: optional) =
    this.internalGetOrDefault(T::class, key)

/**
 * Date time formatter to parse LocalDateTime values. Default is ISO (yyyy-MM-dd HH:mm:ss).
 */
var dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

/**
 * **Internal function.** This function is not meant to be called directly.
 */
@Suppress("UNCHECKED_CAST")
fun <T : Any> JsonObject.internalGetOrDefault(clazz: KClass<T>, key: String, default: T? = null): T? {
    val v = this[key]
    if (v == null || v.isJsonNull) return default
    @Suppress("IMPLICIT_CAST_TO_ANY")
    return when (clazz) {
        Boolean::class -> v.asBoolean
        Int::class -> v.asInt
        Long::class -> v.asLong
        Double::class -> v.asDouble
        String::class -> v.asString
        LocalDateTime::class -> LocalDateTime.parse(v.asString, dateTimeFormatter)
        JsonObject::class -> v.asJsonObject
        JsonArray::class -> v.asJsonArray
        else -> {
            error("Unsupported type: ${clazz.simpleName} ")
        }
    } as T
}

/**
 * Returns a value of JSON path or throws [IllegalAccessException] if the path doesn't exists. See [path] for details.
 */
fun JsonElement.strictPath(path: String): JsonElement = this.path(path, true)!!

/**
 * Returns a value of JSON path or null if the path doesn't exists.
 *
 * The syntax of the path is:
 *
 *   - To refer to an object field, use dot (.) before field name.
 *   - To refer to an array field, use brackets (`[]`).
 *   - For multi dimensional arrays, you can combine the indexing by separating the indexes by comma.
 *
 * For example:
 * The json is:
 *
 * ```
 * {
 *    f1 : 42,
 *    so : {
 *       sof1 : "a",
 *       sof2 : [
 *          "af1", "af2"
 *       ]
 *    },
 *    mda : [
 *      [ "mda11", "mda12" ],
 *      [ "mda21", "mda22", "mda23" ]
 *    ]
 * }
 * ```
 * The path results:
 *
 *   - `f1` -> 42
 *   - `so.sof1` -> "a"
 *   - `so.sof2[1]` -> "af2"
 *   - `mda[0][0]` -> "mda11"
 *   - `mda[1,1]` -> "mda22"
 */
fun JsonElement.path(path: String, strict: Boolean = false): JsonElement? {

    var p = path
    var j = this
    var inArray = false
    var v = ""

    fun extract(trim: Int, vararg endOfToken: String) {
        p = p.substring(trim)
        var i = endOfToken.map { p.indexOf(it) }.filter { it != -1 }.minOrNull() ?: -1
        if (i == -1) {
            if (endOfToken.contains("EOP")) i = p.length
            else throw IllegalArgumentException("End of token not found at ${path.length - p.length} : $path")
        }
        v = p.substring(0, i)
        p = p.substring(i)
    }

    while (p.isNotBlank()) {
        var skip = false
        when {
            p.startsWith("[") -> {
                if (inArray) throw IllegalArgumentException("Nested indexes at ${path.length - p.length} : $path")
                inArray = true
                extract(1, ",", "]")
            }
            inArray && p.startsWith(",") -> {
                extract(1, ",", "]")
            }
            inArray && p.startsWith("]") -> {
                p = p.substring(1)
                inArray = false
                skip = true
            }
            !inArray && p.startsWith(".") -> {
                p = p.substring(1)
                skip = true
            }
            else -> {
                extract(0, ".", "[", "EOP")
            }
        }
        if (skip) continue

        if (inArray) {
            if (j.isJsonArray) {
                val i = v.toIntOrNull()
                    ?: throw IllegalArgumentException("Index ($v) should be a number at ${path.length - p.length} : $path")
                j = j.asJsonArray[i]
            } else {
                if (!strict) return null else throw IllegalArgumentException("Array index on non-array element at ${path.length - p.length} : $path")
            }
        } else {
            if (j.isJsonObject) {
                val o = j.asJsonObject
                if (o.has(v))
                    j = j.asJsonObject[v]
                else
                    if (!strict) return null else throw IllegalArgumentException("Element '$v' not found at ${path.length - p.length} : $path")
            } else {
                if (!strict) return null else throw IllegalArgumentException("Element is not an object at ${path.length - p.length} : $path")
            }
        }
    }
    return j
}
