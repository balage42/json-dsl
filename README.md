# Kotlin DSL for fluent JSON creation

## Introduction

This small DSL helps to create JSON fluently and to query JSON fields typed. Also provided a path matching function to
navigate within a JSON.

## Creating JSON

Creating JSON elements is usually done by `jsonObject` or `jsonArray` builders. By default they create a new entity, but
optionally, both could receive an existing JSON element (matching the type) and extends that element instead of creating
a new one.

Field could be added to the JSON object by the plus assign (`+=`) operator over a string. New items could be added to a
JSON array by the unary, prefix plus (`+`) operator. However, due to the original definition of the unary plus operator
on numbers, they could either passed as strings or by using the `add` function.

For example:

```kotlin
// Creating a new Json
val json1 = jsonObject {
    "f1" += 42
    "f2" += "apple"
}

println(json1)

// Extending the json
val json2 = jsonObject(json1) {
    // Creating a new Json
    val json1 = jsonObject {
        "f1" += 42
        "f2" += "apple"
    }

    println(json1)

    // Extending the json
    val json2 = jsonObject(json1) {
        // You can do anything within
        val v = (Math.random() * 100).toInt()
        "f3" += v
        "f4" += jsonArray {
            // You can iterate
            (0..4).forEach {
                +"i$it"
            }
        }
    }

    println(json2)
}
```

will print:

```json
{
  "f1": 42,
  "f2": "apple"
}
{
  "f1": 42,
  "f2": "apple",
  "f3": 87,
  "f4": [
    "i0",
    "i1",
    "i2",
    "i3",
    "i4"
  ]
}
```

## Formatting JSON

There is a utility function to pretty format JSON elements. It is the `toPretty` which will return a string with the
formatted json.

For example, continuing the above example, the `println(json2.toPretty())` will print:

```json
{
  "f1": 42,
  "f2": "apple",
  "f3": 87,
  "f4": [
    "i0",
    "i1",
    "i2",
    "i3",
    "i4"
  ]
}
```

## Querying JSON

The library provides extension functions over JsonObject to allow typed query of values. This is done by the invoke `()`
operator. The operator comes in three flavors:

- `val v : Int = json("f1")`, will fetch the `f1` field as Int. If the field doesn't exists, it will throw an
  exception: `Json field 'f1' should not be null.`
- `val v : Int = json("f1", -1)`, will fetch the `f1` field as Int. If the field doesn't exists, the provided default
  value (-1) is returned.
- `val v : Int? = json("f1", optional)`, will fetch the `f1` field as Int. If the field doesn't exists, `null` is
  returned.

The `path` and `strictPath` functions allow to browse the JSON along a path. The difference is that `path`returns `null`
if the given path doesn't exist, while `strictPath` throws an exception. The syntax of the path:

- To refer to an object field, use dot (.) before field name.
- To refer to an array field, use brackets (`[]`).
- For multi dimensional arrays, you can combine the indexing by separating the indexes by comma.

Some examples.

Let's take the following JSON:

```json
{
  f1: 42,
  so: {
    sof1: "a",
    sof2: [
      "af1",
      "af2"
    ]
  },
  mda: [
    [
      "mda11",
      "mda12"
    ],
    [
      "mda21",
      "mda22",
      "mda23"
    ]
  ]
}
```

The following would be the result of path definitions:

| Path expression | Result |
| --------------- | ------ |
| `f1`            | 42     |
| `so.sof1`       | a      |
| `so.sof2[1]`    | af2    |
| `mda[0][0]`     | mds11  |
| `mda[1,1]`      | mda22  |

